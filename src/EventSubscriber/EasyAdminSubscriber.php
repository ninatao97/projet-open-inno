<?php

namespace App\EventSubscriber;

use App\Entity\Categorie;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\String\Slugger\SluggerInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{

    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }


    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setCategorieSlug'],
        ];
    }

    public function setCategorieSlug(BeforeEntityPersistedEvent $event)
    {
        /** @var BeforeEntityPersistedEvent $event */
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Categorie)) {
            return;
        }

        $slug = $this->slugger->slug($entity->getNom());
        $entity->setSlug($slug);
    }
}
