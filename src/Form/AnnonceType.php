<?php

namespace App\Form;

use App\Entity\Annonce;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                "label" => "Nom d'annonce",
                "attr" => ['placeholder' => 'Taper le nom d\'annonce'],
                'required' => true,
            ])
            ->add('type', ChoiceType::class, [
                // TODO: Définir le type d'annonce
                'label' => 'Image du produit',
                'attr' => [
                    'placeholder' => "Taper une URL d\'image"
                ],
                'required' => false,
            ])
            ->add('categorie', EntityType::class, [
                'label' => 'Catégorie d\'annonce',
                'attr' => [],
                'placeholder' => '-- Choisir une catégorie --',
                'class' => Categorie::class,
                'choice_label' => function (Categorie $categorie) {
                    return strtoupper($categorie->getNom());
                }
            ])
            ->add('description', TextareaType::class, [
                "label" => "Description courte",
                "attr" => [
                    'placeholder' => 'Taper une description courte d\'annonce'
                ],
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
        ]);
    }
}
