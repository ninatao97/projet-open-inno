<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Repository\AnnonceRepository;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnnoncesController extends AbstractController
{
    private $annonceRepository;

    public function __construct(
        AnnonceRepository $annonceRepository
    ) {
        $this->annonceRepository = $annonceRepository;
    }

    /**
     * @Route("/home", name="homepage")
     */
    public function showAllAnnonces(): Response
    {
        $annonces = $this->annonceRepository->findAll();

        return $this->render('acceuil.html.twig', [
            'annonces' => $annonces,
        ]);
    }

    /**
     * @Route("/annonces/{slug}", name="annonces_par_categorie")
     */
    public function showAnnoncesParCategorie(
        $slug,
        CategorieRepository $categorieRepository
    ): Response {
        $categorie = $categorieRepository->findOneBy([
            'slug' => $slug
        ]);

        if (!$categorie) {
            throw $this->createNotFoundException("La catégorie demandée n'existe pas");
        }
        return $this->render('annonces/categorie.html.twig', [
            "category" => $categorie
        ]);
    }


    /**
     * @Route("/annonces/{id}", name = "show_annonce_detail")
     */
    public function showAnnonceDetail($id): Response
    {
        $annonce = $this->annoncesRepository->findOneBy([
            'id' => $id
        ]);
        if (!$annonce) {
            throw $this->createNotFoundException("L'annonce demandée n'existe pas");
        }

        return $this->render('annonce/annonce.html.twig', [
            'annonce' => $annonce,
        ]);
    }

    /**
     * @Route("/admin/annonce/create", name="create_annonce")
     */
    public function createAnnonce(FormFactoryInterface $factory, Request $request, SluggerInterface $slugger, EntityManagerInterface $manager)
    {
        /** @var Annonce $annonce */
        $annonce = new Annonce;
        $form = $this->createForm(AnnonceType::class, $annonce);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getUser();
            $annonce->setPosteur($user);

            $manager->persist($annonce);
            $manager->flush();
        }

        $formView = $form->createView();


        return $this->render('annonce/annonce_create.html.twig', [
            'formView' => $formView
        ]);
    }
}
